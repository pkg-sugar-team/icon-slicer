/*
 * Copyright © 2003 Red Hat, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Red Hat not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Red Hat makes no representations about the
 * suitability of this software for any purpose.  It is provided "as is"
 * without express or implied warranty.
 *
 * RED HAT DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL RED HAT
 * BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Author:  Owen Taylor, Red Hat, Inc.
 */
#ifndef __THEMEFILE_H__
#define __THEMEFILE_H__

#include <gdk-pixbuf/gdk-pixbuf.h>

G_BEGIN_DECLS

typedef struct _ThemeCursor ThemeCursor;
typedef struct _ThemeAlias ThemeAlias;
typedef struct _ThemeDisplayName ThemeDisplayName;
typedef struct _ThemeFrame ThemeFrame;
typedef struct _ThemeFrameConfig ThemeFrameConfig;
typedef struct _ThemeIcon ThemeIcon;
typedef struct _ThemeIconInstance ThemeIconInstance;
typedef struct _ThemeImage ThemeImage;
typedef struct _ThemeLocation ThemeLocation;
typedef struct _ThemePoint ThemePoint;
typedef struct _ThemeRectangle ThemeRectangle;
typedef struct _ThemeSource ThemeSource;
typedef struct _ThemeFile ThemeFile;

typedef enum {
  THEME_ALIAS_CURSOR,
  THEME_ALIAS_ICON
} ThemeAliasType;

struct _ThemeLocation
{
  int row;
  int column;
};

struct _ThemeCursor
{
  ThemeLocation location;
  
  char *name;
  GSList *frame_configs;
  GSList *frames;
};

struct _ThemeDisplayName
{
  char *lang;
  char *str;
};

struct _ThemeIcon
{
  ThemeLocation location;

  char *name;
  char *typedir;
  GSList *display_names;
  GSList *instances;
};

struct _ThemeAlias
{
  char *name;
  char *target;
  char *typedir;
  GSList *display_names;
};

struct _ThemeFrameConfig
{
  int delay;
};

struct _ThemeFrame
{
  int size;
  int hot_x;
  int hot_y;
  int delay;
  GdkPixbuf *image;
};

struct _ThemePoint
{
  int x;
  int y;
};

struct _ThemeRectangle
{
  int x;
  int y;
  int width;
  int height;
};

struct _ThemeIconInstance
{
  ThemeSource *source;
  
  ThemeRectangle *embedded_rect;
  GSList *attach_points;

  GdkPixbuf *image;
};

#define THEME_SOURCE_USE_HOTSPOT -1
#define THEME_SOURCE_USE_EMBEDDED_RECT -2
#define THEME_SOURCE_USE_ATTACH_POINTS -3

struct _ThemeImage
{
  int use;			/* Frame number or magic value */
  GdkPixbuf *image;
};

struct _ThemeSource
{
  int size;
  int gridsize;
  int margin;
  int spacing;
  char *sizedir;
  GSList *images;
};

struct _ThemeFile
{
  char *name;
  char *typedir;
  GSList *sources;
  GHashTable *cursors;
  GHashTable *icons;
  GHashTable *aliases;
};

ThemeFile *theme_file_read (const char *filename,
			    const char *image_dir);
void       theme_file_free (ThemeFile *theme_file);

G_END_DECLS

#endif /* __THEMEFILE_H__ */
