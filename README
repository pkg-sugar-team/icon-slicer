Introduction
============

icon-slicer is a utility for generating icon themes and libXcursor
cursor themes.

The inputs to icon-slicer are conceptually:
 
 A) A set of multi-layer images, one for each size.
 B) A XML theme description file

Each image contains all the cursors arranged in a grid; For 
cursors the layers are:
 
 - A layer with a dot for the hotspot of each cursor
 - The main image or first animation frame for multi-frame
   animated cursors
 - The second animation frame for multi-frame animated cursors
 - ...

For icons, the layers are:

 - A layer with the images
 - An optional layer with attachment points for emblems 
 - An optional layer with boxes for embedding text into 
   icons.

In practice, since loading of multilayer images is not supported by
standard image libraries, each layer is input as a separate image
file.

The theme description file contains, among other things, information about 
the source  images to read, the location of each named cursor or icon 
within the grid, and a set of aliases from names to other names.


Installation
============

icon-slicer requires GTK+-2.0 or newer for the gdk-pixbuf image
manipulation library. It probably is already on your system; if not,
it is available from ftp://ftp.gtk.org. It also requires the
popt library from the RPM distribution. (Should be commonly
available on most Linux distributions, even if they don't use RPM.)

Once you have those installed, building icon-slicer is a simple
./configure ; make ; make install. If you installed GTK+ in a
non-standard location, say /opt/gtk, then you will need to set
PKG_CONFIG_PATH to include /opt/gtk/lib/pkgconfig.


Invocation
==========

icon-slicer takes some number of theme description files as input,
along with options:

  --output-dir=DIRECTORY
    The directory into which to write output. Mandatory.

  --image-dir=DIRECTORY
    The directory in which to find source images. If not
    specified, the current working directory is used.


Theme file format
=================

The tags in the Theme file format are:

<theme>: The toplevel element. There must be
  exactly one of these.

  Attributes:
    name: The name of the theme. Required.

    typedir: default type portion of output directory for icons
      in this theme.

  Child elements:
    <alias>, <layout>, <source>

<alias>: The alias element defines an alias of  one cursor name to 
  another cursor or of one icon name to an another icon. An icon 
  alias can also have a display name associated which overrides the
  display name of the target alias.

  Attributes: 
   name: The name of the alias. Required.
   target: The name cursor (or alias) to which the
      alias points. Required.
    typedir: type portion of output directory. Optional

  Child elements: 
    DisplayName

<source>: Defines a multilayer source

 Attributes:
    size: The nominal size of the cursors in the
      this source. This is used when selecting a 
      cursor. Required.

    sizedir: size portion of output directory name for 
      icons. Optional.

    gridsize: The size of the grid used for this
      source. Defaults to the same as 'size'.
      Optional.

    margin: border around the entire source outside of
     all grid cells. Optional.

    spacing: spacing between the cells of the grid. Optional.

 Child elements:
    <image>

<image>: One layer in a multilayer source

  Attributes:
    file: The filename to get the image from. Required.
    use: Either an integer indicating what animation
     frame this is (0, 1, ...) or "hotspot" to
     indicate that this frame contains hotspots. If not
     present, the same as giving '0'

  Child elements:
    None

<layout>: Defines the layout of cursors or icons within the grid.
  
  Attributes:
    None
 
  Child elements:
    <row>

<row>: Defines one row of cursors or icons within the grid

  Attributes:
    None
 
  Child elements:
    <cursor>
    <icon>

<cursor>: Defines a single cursor. If there are no
  child <frame> elements, it is assumed to be a non-animated
  cursor with a single frame in it. Otherwise, one
  or more frames is specified with child <frame> elements.

  Attributes:
    name: The name of the cursor. Required.
 
  Child elements:
    <frame>

<frame>: Defines one frame of a multiframe cursor.
  
  Attributes:
    delay: The delay before the next frame is shown in 
    milliseconds. Optional. (If you don't specify the delays 
    for a multiframe animation, the result is undefined.)

  Child elements:
    None

<icon>: Defines a single icon.

  Attributes:
    name: The name of the icon. Required.

    typedir: type portion of output directory. Optional
 
  Child elements:
    displayname

<displayname>: Defines a possibly translated display name 
   for an icon or an alias.

  Attributes:
    str: The untranslated value of the display name
 
  Child elements:
    locale

<locale>: Defines one translated value for a <displayname> element

  Attributes:
    lang: Language for which this is a translation
    str: The translated value of the display name
 
  Child elements:
    None

Note that icon-slicer doesn't actually use a full XML parser, but
the XML-subset GMarkup parser from GLib, so some less common XML
constructs may not work.


Note about icon output location
===============================

Cursors are always output in [outputdir]/cursors; for icons
and icon aliases, it's a bit more complicated.

Define [sizedir] to be the 'sizedir' attribute of
the applicable <source> element, if any. [typedir] to be
the 'typedir' attribute of the icon or alias, if any, 
or the 'typedir' attribute specified for the <theme>
element if not specified for the icon or alias. Then
the output directory is:

 [outputdir]/[sizedir]/[typedir]

Where either sizedir or typedir is allowed to be empty,
but not both.


Makefile example
================

Makefile rules for generating and installing a theme look like:

sample_sources =				\
	sample.cursortheme			\
	sample-cursors-24-1.png			\
	sample-cursors-24-hotspot.png		\
	sample.icontheme			\
	sample-icons-24.png			\
	sample-icons-16.png

Sample.stamp: $(sample_sources) $(icon_slicer)
	rm -rf Sample					\
	&& mkdir Sample					\
	&& icon_slicer					\
		--output-dir=Sample			\
		--image-dir=$(srcdir) 			\
		$(srcdir)/sample.cursortheme 		\
		$(srcdir)/sample.icontheme		\
	&& touch Sample.stamp

install:
	rm -rf $(datadir)/icons/Sample
	dirs=`find Sample -type d -print` ; \
	for d in $$dirs ; do                \
	  mkdir -p $(datadir)/icons/$$d ;   \
        done
	files=`find Sample -type f -print` ;         \
	for f in $$files ; do                        \
	  install -m 0644 $$f $(datadir)/icons/$$f ; \
        done
	links=`find Sample -type l -print` ; \
	for l in $$links ; do                \
	  cp -d $$l $(datadir)/icons/$$l ;   \
        done

uninstall:
	rm -rf $(datadir)/icons/Sample

License
=======

Copyright © 2003 Red Hat, Inc.

Permission to use, copy, modify, distribute, and sell this software and its
documentation for any purpose is hereby granted without fee, provided that
the above copyright notice appear in all copies and that both that
copyright notice and this permission notice appear in supporting
documentation, and that the name of Red Hat not be used in advertising or
publicity pertaining to distribution of the software without specific,
written prior permission.  Red Hat makes no representations about the
suitability of this software for any purpose.  It is provided "as is"
without express or implied warranty.

RED HAT DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL RED HAT
BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

